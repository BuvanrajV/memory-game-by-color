function startGame() {
  let letsStart = document.getElementById("section-lets-start");
  letsStart.style.display="none";

  let startGame = document.getElementById("section-game-start-before");
  // startGame.removeAttribute("id");
  startGame.style.display="block"
}

function goBack(){
  let startGame = document.getElementById("section-game-start-before");
  startGame.style.display="none"

  let letsStart = document.getElementById("section-lets-start");
  letsStart.style.display="block";
  letsStart.style.display="flex"
}

const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
];

function shuffle(array) {
  let counter = array.length;
  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    const newDiv = document.createElement("div");
    newDiv.classList.add(color);
    newDiv.style.backgroundColor = "whitesmoke";
    newDiv.addEventListener("click", handleCardClick);
    gameContainer.append(newDiv);
  }
}
let clickedBefore;
let clickedBeforeEvent;
let count = 0;

function handleCardClick(event) {
  let colorDiv = event.target.classList[0];
  if (count < 2 && event.target.style.backgroundColor == "whitesmoke") {
    count++;
    if (count === 1) {
      event.target.style.backgroundColor = colorDiv;
      clickedBefore = colorDiv;
      clickedBeforeEvent = event;
      console.log(`count : ${count}`);
    } else {
      console.log(`Clicked before : ${clickedBefore}`);
      event.target.style.backgroundColor = colorDiv;
      console.log(colorDiv + " Clicked");
      setTimeout(() => {
        if (clickedBefore !== colorDiv) {
          event.target.style.backgroundColor = "whitesmoke";
          clickedBeforeEvent.target.style.backgroundColor = "whitesmoke";
        }
        count = 0;
      }, 500);
    }
  }
}

createDivsForColors(shuffledColors);

